//
// Created by Matteo Favaro on 21/12/2016.
//
#include "SensorManager.h"

SensorManager::SensorManager(RF24Network &network) : network_(network) {
  firstFreeAddressForSensor = rfAddressEEPROMAddress + sizeof(uint16_t);
}


void SensorManager::begin() {

  //check if we are in uncofigured mode
  this->network_.begin(/*channel*/ DEFAULT_CHANNEL, /*node address*/
                            this_node_address);

}