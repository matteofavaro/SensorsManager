//
// Created by Matteo Favaro on 21/12/2016.
//

#ifndef SENSORMANAGER_H
#define SENSORMANAGER_H

#include <EEPROM.h>
#include "Message.h"
#include "RF24Network.h"



uint16_t const configuredEEPROMAddress = 0;
uint16_t const rfAddressEEPROMAddress = 1; //uint16_t



class SensorManager {
  //Memory management
  uint16_t nextMemoryAccess;
  // Network STUFF
  RF24Network &network_;
  RF24NetworkHeader lastNetworkHeader;

  // LOGICAL SENSOR
  Sensor *sensorOnThisArduino[MAX_SENSOR_PER_ARDUINO];
  Message message_;

 public:
  SensorManager( RF24Network &_network);
  bool addSensor(Sensor *sensor);

  bool updateSensorsStatus();
  bool sensorsHaveToSend();
  RawMessage *getRawMessage();

  Sensor *getSensors(uint8_t index);
  uint8_t getSensorsNum();


  //NETWORK STUFF
  void begin(RF24Network &network);
  uint8_t receiveMessage(MessageHelper * message);
  uint8_t updateNetwork();
  bool networkAvailable();
  bool sendMessage(Sensor *fromSensor,
                   Sensor *toSensor,
                   RawMessage const *message);

 private:
  void setNodeAddress(uint16_t address);



};





#endif //SENSORMANAGER_H
